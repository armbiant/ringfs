(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module H : Hashtbl.HashedType with type t = int = struct
   type t = int
   let equal = (=)
   let hash = Hashtbl.hash
end

let test (module Cache: Aches_lwt.Lache.MAP_OPTION with type key = int) =
   let c = Cache.create 5 in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in

   let () = Cache.put c 0 (Lwt.return_none) in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in

   let (p, u) = Lwt.wait () in
   let () = Cache.put c 0 p in
   let () = assert (Cache.length c = 1) in
   let () = Lwt.wakeup u None in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in

   let (p, u) = Lwt.wait () in
   let () = Cache.put c 0 p in
   let () = assert (Cache.length c = 1) in
   let () = Lwt.wakeup u (Some "zero") in
   let () = assert (Cache.length c = 1) in
   let p1 = Cache.take c 0 in
   let p1 = Option.get p1 in
   let () = assert (Lwt.state p1 = Lwt.Return (Some "zero")) in

   let (p, u) = Lwt.wait () in
   let () = Cache.put c 0 p in
   let () = assert (Cache.length c = 1) in
   let pp = Cache.bind c 0 Lwt.return in
   let pp = Option.get pp in
   let () = Lwt.wakeup u None in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in
   let () = assert (Lwt.state p = Lwt.Return None) in
   let () = assert (Lwt.state pp = Lwt.Return None) in

   let (p, u) = Lwt.wait () in
   let () = Cache.put c 0 p in
   let () = assert (Cache.length c = 1) in
   let pp = Cache.bind c 0 Lwt.return in
   let pp = Option.get pp in
   let () = Cache.remove c 0 in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in
   let () = assert (Lwt.state p = Lwt.Sleep) in
   let () = assert (Lwt.state pp = Lwt.Sleep) in
   let () = Lwt.wakeup u (Some "zero") in
   let () = assert (Lwt.state p = Lwt.Return (Some "zero")) in
   let () = assert (Lwt.state pp = Lwt.Return (Some "zero")) in

   ()

let test (module R: Aches.Rache.TRANSFER with type key = int) =
  let module C = Aches_lwt.Lache.Make_option(R) in
  test (module C : Aches_lwt.Lache.MAP_OPTION with type key = int)

open Aches.Rache
let () = test (module Transfer(LRU)(H))
let () = test (module Transfer(FIFO)(H))
