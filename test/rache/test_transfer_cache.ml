(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module H : Hashtbl.HashedType with type t = int = struct
   type t = int
   let equal = (=)
   let hash = Hashtbl.hash
end
module Resource = struct
  let repository = ref []
  let make s =
    let r = ref s in
    repository := r :: !repository;
    r
  let destroyed = "destroyed"
  let destroy _ r = assert (!r <> destroyed); r := destroyed
  let check_all_destroyed () =
    assert (List.for_all (fun r -> !r = destroyed) !repository)
end

let test (module Cache: Rache.TRANSFER with type key = int) =
   let c = Cache.create Resource.destroy 5 in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in
   let () = assert (Cache.borrow c 0 (fun _ -> assert false) = None) in

   let r0 = Resource.make "zero" in
   let () = Cache.put c 0 r0 in
   let () = assert (Cache.length c = 1) in
   let () = assert (Cache.borrow c 0 (fun r -> assert (!r = "zero")) = Some ()) in

   let r1 = Resource.make "one" in
   let () = Cache.put c 1 r1 in
   let () = assert (Cache.length c = 2) in
   let () = assert (Cache.borrow c 1 (fun r -> assert (!r = "one")) = Some ()) in

   let rx = match Cache.take c 0 with
     | None -> assert false
     | Some rx -> assert (!rx = "zero"); rx
   in
   let () = assert (Cache.length c = 1) in

   let () = Cache.clear c in
   let () = assert (Cache.length c = 0) in

   let () = Resource.destroy 0 rx in
   let () = Resource.check_all_destroyed () in

   let () = Cache.put c 10 (Resource.make "10") in
   let () = Cache.put c 11 (Resource.make "10") in
   let () = Cache.put c 12 (Resource.make "10") in
   let () = Cache.put c 15 (Resource.make "15") in
   let () = Cache.put c 16 (Resource.make "16") in
   let () = Cache.put c 17 (Resource.make "17") in
   let () = Cache.put c 18 (Resource.make "18") in
   let () = Cache.put c 19 (Resource.make "19") in

   let () = assert (Cache.length c = 5) in

   let rs = Cache.take_some c (fun k _ -> k = 15 || k = 17) in
   let () = assert (List.compare_length_with rs 2 = 0) in
   let () = assert (Cache.length c = 3) in

   let () = Cache.clear c in
   let () = assert (Cache.length c = 0) in
   let () = List.iter (fun (k, v) -> Resource.destroy k v) rs in
   let () = Resource.check_all_destroyed () in

   ()

let () = test (module Rache__Functors.Transfer (Ringo.LRU_Collection) (H))
let () = test (module Rache.Transfer (Rache.LRU) (H))
let () = test (module Rache__Functors.Transfer (Ringo.FIFO_Sloppy_Collection) (H))
let () = test (module Rache__Functors.Transfer (Ringo.FIFO_Precise_Collection) (H))
let () = test (module Rache.Transfer (Rache.FIFO) (H))


let test_empty () =
   let module Cache = Rache.EmptyTransferMap (H) in
   let c = Cache.create Resource.destroy (-1) in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.borrow c 0 (fun _ -> assert false) = None) in
   let () = Cache.put c 0 (Resource.make "0") in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.borrow c 0 (fun _ -> assert false) = None) in
   let () = Resource.check_all_destroyed () in
   ()
let () = test_empty ()

let test_single () =
   let module Cache = Rache.SingletonTransferMap (H) in
   let c = Cache.create Resource.destroy (-1) in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.borrow c 0 (fun _ -> assert false) = None) in
   let () = Cache.put c 0 (Resource.make "0") in
   let () = assert (Cache.borrow c 0 (fun r -> assert (!r = "0")) = Some ()) in
   let () = assert (Cache.length c = 1) in
   let () = Cache.remove c 0 in
   let () = assert (Cache.length c = 0) in
   let () = Resource.check_all_destroyed () in
   ()
let () = test_single ()
