(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 Vache} *)

(** Vache is a library for in-memory value caches.

    More specifically, Vache provides modules implementing caches for values.
    These caches are parametrised by cache-policies: maximum size, retention,
    etc.

    Note that Vache should not be used to cache resources which may need any
    form of clean-up (e.g., file-descriptors or network connections). Vache
    should only be used for values which can be entirely managed by the
    garbage-collector. If you need resource caches, check-out Rache.
*)

(** {2 Preamble} *)

(** {2 Caches} *)

module type MAP = Sigs.MAP
module type SET = Sigs.SET

(** All caches of Vache have either the {!MAP} interface (for key-value
    stores) or the {!SET} interface (for value stores). Their behavior can
    be tweaked by the parameters below. *)

(** {2 Cache policies} *)

(** [REPLACEMENT_AND_ACCOUNTING] is for defining the replacement policy and the
    accounting policy of a cache. Because of implementation details which are
    not relevant to go into details here, these two policies are governed by a
    single, joined parameter.

    Replacement:
    [LRU] is for "Least Recently Used", meaning that when a supernumerary item
    is inserted in the cache, the least recently used item is removed to make
    room.
    [FIFO] is for "First-In, First-Out" meaning that when a supernumerary item
    is inserted in the cache, the oldest inserted element is removed to make
    room.

    Accounting:
    [Precise] means that the cache counts its number of elements precisely: when
    an element is added, the count increases, when an element is removed, the
    count decreases, when an already present element is re-inserted the count
    is unchanged.
    [Sloppy] means that the cache may count some elements that are not in the
    cache as still being held by the cache. As a result, adding a new element
    into the cache may lead to another element being removed even though the
    size limit was not actually reached.

    The element-count of a [Sloppy] cache may become offset by one when an
    element is [remove]d or when an element that is already present in the cache
    is re-[add]ed. This effect is cumulative and the element count can be off by
    more than one if multiple, say, removals happen.

    The element-count eventually restores itself if enough new elements are
    [add]ed.

    Additional details of the accounting in [Sloppy] caches are implementation
    dependent. Use [Precise] only if you use [remove] a lot, if you might insert
    the same key multiple times often, or if you need strong guarantees on the
    number of elements.

    Note that when requesting a [Sloppy] cache, the library might give you a
    [Precise] cache if there is no additional runtime cost. In general, [Sloppy]
    caches are more efficient, but depending on the other parameters they might
    be only as-efficient-as (not strictly more efficient than) [Precise] caches. *)
module type REPLACEMENT_AND_ACCOUNTING
module LRU_Precise : REPLACEMENT_AND_ACCOUNTING
module LRU_Sloppy : REPLACEMENT_AND_ACCOUNTING
module FIFO_Precise : REPLACEMENT_AND_ACCOUNTING
module FIFO_Sloppy : REPLACEMENT_AND_ACCOUNTING

(** [OVERFLOW] is for defining the overflow policy of a cache. [Strong] means
    that the cache never holds more element than is specified when calling
    [create]. [Weak] means that the cache may hold more elements than
    specified when calling [create] but that supernumerary elements may be
    collected by the Garbage Collector. *)
module type OVERFLOW
module Strong : OVERFLOW
module Weak : OVERFLOW


(** {2 Cache instantiating} *)

(** [Map(RA)(O)(H) is a [MAP] indexed by [H] and governed by [RA] and [O]. *)
module Map
  (RA: REPLACEMENT_AND_ACCOUNTING)
  (O: OVERFLOW)
  (H: Hashtbl.HashedType)
: MAP with type key = H.t

(** [EmptyMap(H)] is a map module but it only supports the empty map: a map with
    zero elements.

    The [create] function ignores its size-limit parameter: the size limit is
    hardcoded.

    Note that all policies are equivalent in the case of an empty map. This is
    why the empty map does not require the user to specify any policy. *)
module EmptyMap (H: Hashtbl.HashedType)
: MAP with type key = H.t

(** [SingletonMap(H)] is a map module but it only supports singleton maps: maps
    with at most one element.

    The [create] function ignores its size-limit parameter: the size limit is
    hardcoded.

    Note that all policies are equivalent in the case of a singleton map. This
    is why the singleton map does not require the user to specify any policy. *)
module SingletonMap (H: Hashtbl.HashedType)
: MAP with type key = H.t

(** [Set(RA)(O)(H) is a [SET] indexed by [H] and governed by [RA] and [O]. *)
module Set
  (RA: REPLACEMENT_AND_ACCOUNTING)
  (O: OVERFLOW)
  (H: Hashtbl.HashedType)
: SET with type elt = H.t

(** [EmptySet(H)] is a set module but it only supports the empty set: a set with
    zero elements.

    The [create] function ignores its size-limit parameter: the size limit is
    hardcoded.

    Note that all policies are equivalent in the case of an empty set. This is
    why the empty set does not require the user to specify any policy. *)
module EmptySet (H: Hashtbl.HashedType)
: SET with type elt = H.t

(** [SingletonSey(H)] is a set module but it only supports singleton sets: sets
    with at most one element.

    The [create] function ignores its size-limit parameter: the size limit is
    hardcoded.

    Note that all policies are equivalent in the case of a singleton set. This
    is why the singleton set does not require the user to specify any policy. *)
module SingletonSet (H: Hashtbl.HashedType)
: SET with type elt = H.t
