(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** [Map(C)(T)(H)] is a [Sigs.MAP]. The exact behavior of the cache
    is determined by [C] and [T]. Specifically, the replacement policy of the
    cache is determined by [C] (see available collections bellow) and the
    strength of the size-bound is determined by [T] (see available tablers
    bellow). *)
module Map
  (Collection: Ringo.COLLECTION)
  (Tabler: Sigs.TABLER)
  (H: Hashtbl.HashedType)
  : Sigs.MAP with type key = H.t

(** [Set(C)(T)(H)] is a [Sigs.SET]. The exact behavior of the cache
    is determined by [C] and [T]. Specifically, the replacement policy of the
    cache is determined by [C] (see available collections bellow) and the
    strength of the size-bound is determined by [T] (see available tablers
    bellow). *)
module Set
  (Collection: Ringo.COLLECTION)
  (Tabler: Sigs.TABLER)
  (H: Hashtbl.HashedType)
  : Sigs.SET with type elt = H.t
