(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Aches-lwt: a variety of caches for promises

    Aches_lwt is a library that provides caches (limited-size collections with
    automatic discarding of supernumerary elements) for a variety of uses around
    Lwt promises.

    Lwt promises are I/O-dependent values which can take time to resolve. As
    such, using them in a more standard value cache (e.g., as provided by Aches)
    can lead to race conditions. The most common scenario leading to a race
    condition is because of the possible delay between testing whether a value
    is held in a cache and resolving a fresh value:

    {[
      (* with a normal cache holding non-promise values *)
      match find_opt c k with
      | None ->
          (* value is not there *)
          let* v = … in (* make missing values *)
          (* oops the Lwt scheduler has kicked in and there is delay here *)
          replace c k v; (* insert *)
          … (* use [v] *)
      | Some v ->
          … (* use [v] *)
    ]}

    Aches-lwt avoids these race conditions by working directly with promises in
    the backend, providing a carefully selected API and maintaining a strict
    cancelling discipline. *)

(** Lwt-promise caches *)
module Lache = Lache
