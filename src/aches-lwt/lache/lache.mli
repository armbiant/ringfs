(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 Lache} *)

(** Lache is a library for Lwt-promise caches.

    More specifically, Lache provides modules implementing caches for Lwt
    promises. These caches are based on [Aches.Rache]'s Transfer-cache but store
    promises.

    Note that Lache should not be used to cache resources which may need any
    form of clean-up (e.g., file-descriptors or network connections). Lache
    should only be used for promises. These promises are canceled when they are
    purged from the cache.
*)

(** {2 Caches} *)

(** [MAP] are caches for plain promises.

    The cache is automatically cleaned of promises that are rejected (that is,
    that fail with an exception).

    The cache also cancels promises that it purges out for reasons of space. *)
module type MAP = Sigs.MAP

(** [MAP_OPTION] are caches for option promises ([_ option Lwt.t]).

    The cache is automatically cleaned of promises that are rejected (that is,
    that fail with an exception) and of those that resolve to [None]. In that
    way, the cache is opinionated: it considers [option] as a kind of error
    management strategy where [None] is not a value worth keeping.

    The cache also cancels promises that it purges out for reasons of space. *)
module type MAP_OPTION = Sigs.MAP_OPTION

(** [MAP_RESULT] are caches for result promises ([_ result Lwt.t]).

    The cache is automatically cleaned of promises which are rejected (that is,
    promises which fail with an exception) and of those that resolve to
    [Error _]. In that way, the cache is opinionated: it considers [result] as a
    kind of error management strategy where [Error _] is not a value worth
    keeping.

    The cache also cancels promises that it purges out for reasons of space. *)
module type MAP_RESULT = Sigs.MAP_RESULT

(** {2 Functors} *)

(** [Make(C)] is a {!MAP}. The cache policies are that of [C]. *)
module Make
  (C: Rache.TRANSFER)
: MAP with type key = C.key

(** [Make_option(C)] is a {!MAP_OPTION}. The cache policies are that of [C]. *)
module Make_option
  (C: Rache.TRANSFER)
: MAP_OPTION with type key = C.key

(** [Make_result(C)] is a {!MAP_RESULT}. The cache policies are that of [C]. *)
module Make_result
  (C: Rache.TRANSFER)
: MAP_RESULT with type key = C.key
