(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module type HS = sig

   (** A subset of {!Hashtbl.S}. *)

    type key
    type 'a t
    val create: int -> 'a t
    val remove: 'a t -> key -> unit
    val find_opt: 'a t -> key -> 'a option
    val replace : 'a t -> key -> 'a -> unit
    val length: 'a t -> int
    val clear: 'a t -> unit

end

module type TABLER = functor (H: Hashtbl.HashedType) -> (HS with type key = H.t)

(** {1 Caches}

    Lache is a cache library for Lwt promises. Below are signatures for caches.

    A [MAP] is a collection of key-promise bindings.
    A [MAP_OPTION] is a collection of key-promise bindings for the option type.
    A [MAP_RESULT] is a collection of key-promise bindings for the result type.

    All of these caches handle errors (exceptions, [None], [Error]) by removing
    the promise from the cache.

    All of these caches maintain a strict discipline of canceling promises in
    specific conditions. Read the documentation and adhere to the mentioned
    discipline. (The discipline is somewhat similar to that of ownership in
    {!Rache}, so this may be of interest.)

    Note that there is an important conceptual and practical distinction between
    promises and the value they resolve to. Keep this in mind when you read the
    documentation. Of note: the cache owns the promises, but once a promise is
    resolved to a value, that value can be shared just like any other value. *)


module type MAP = sig

  (** A Mutable structure akin to a hash-table, but with a size bound and for
      storing promimses. Note that, different caches have different policies
      towards the size bounds: some uphold the bound strictly, some treat the
      bound as a suggestion. In addition, some caches count their elements
      somewhat sloppily.

      In general, the caches of Aches are intended to be used in settings that
      do not require strict, by-the-number, extremely-predictable behaviors.

      See [Lache] (or [Functors]) for more information. *)

  (** The type of keys on which values in the cache are indexed. *)
  type key

  (** The type of caches holding bindings from [key] to ['a Lwt.t] *)
  type 'a t

  (** [create n] creates a cache with a size-bound of [n]. Remember that the
      size-bound is not upheld strictly by all caches. *)
  val create : int -> 'a t

  (** [put c k p] binds the key [k] to the promise [p] in the cache [c].
      This transfer the responsibility for [p] to [c]: [c] will cancel the
      promise if it is ever [remove]d, [clear]ed, [put], etc.

      If [k] is already bound to a promise [p'] in [c], the previous binding
      disappears and is replaced by the new binding to [p]. If this happens then
      [p'] is canceled.

      If [k] is not already bound in [c], then [put] adds a new binding from
      [k] to [p]. This may or may not cause another binding to be removed from
      the cache, depending on the number of bindings already present in the
      cache [c], the size-bound of the cache [c], and the policy of the cache
      [c] towards its size-bound. If a supernumerary binding is removed, its
      promise is canceled. *)
  val put : 'a t -> key -> 'a Lwt.t -> unit

  (** [take c k] removes the binding of [k] from [c] and returns it to the
      caller along with the responsibility for the promise: the cache will not
      cancel the promise.

      If [k] is not bound in [c], then [take c k] does nothing. *)
  val take : 'a t -> key -> 'a Lwt.t option

  (** [take_all c] *)
  val take_all : 'a t -> (key * 'a Lwt.t) list

  (** [take_some c f] *)
  val take_some : 'a t -> (key -> bool) -> (key * 'a Lwt.t) list

  (** [bind c k f] waits for the promise bound to [k] in [c] to resolve and
      applies [f] to the resolved value. In more hand-wavy words: [bind c k f]
      is similar to [Lwt.bind (find c k) f].

      If [k] isn't bound in [c], then it returns [None].

      When you call [bind c k f] you take shared-responsibility for the promise.
      From this there is an important remark:

      The promise's ownership is shared. For this reason the cache will not
      cancel the promise, even if the key-promsie binding is removed from the
      cache because a supernumerary binding is inserted. If this happens the
      cache simply lets go of the promise and you become the sole owner. There
      is no mechanism to be notified that you become the sole owner. If you need
      stricter rules of ownership you need to restrict yourself to using [put]
      and [take] only.

      Note that the cache is designed to gracefully handle cancelation of the
      promises inside. And so you can cancel a promise you called [bind] on. *)
  val bind : 'a t -> key -> ('a -> 'b Lwt.t) -> 'b Lwt.t option

  (** [bind_or_put c k mk f] is identical to [bind c k f] if [k] is bound in
      [c].

      If [k] is not bound in [c], then the promise [mk k] is created. It is
      bound to [k] in [c]. Its responsibility is shared by the cache and the
      caller as per the documentation of [bind] above. *)
  val bind_or_put : 'a t -> key -> (key -> 'a Lwt.t) -> ('a -> 'b Lwt.t) -> 'b Lwt.t

  (** [fold f c init] folds the function [f] and value [init] over the keys and
      values that the bindings of [c] resolve to.

      Note that for some caches, this function may fold over a subset of the
      bindings of [c]. Specifically, on caches with a [Weak] overflow policy,
      only the strongly-held elements are folded over. *)
  val fold : (key -> 'a -> 'b -> 'b Lwt.t) -> 'a t -> 'b -> 'b Lwt.t

  (** [fold_oldest_first] is like [fold] but in reversed order: oldest elements
      of [c] first. This function has the same limitation as [fold]. *)
  val fold_oldest_first : (key -> 'a -> 'b -> 'b Lwt.t) -> 'a t -> 'b -> 'b Lwt.t

  (** [remove c k] removes the binding from [k] in [c] and cancels its promise
      (if it hasn't resolved yet).

      If [k] is not bound in [c], it does nothing. *)
  val remove : 'a t -> key -> unit

  (** [clear c] removes all bindings from [c], canceling all unresolved promises. *)
  val clear : 'a t -> unit

  (** [filter c f] *)
  val filter : 'a t -> (key -> bool) -> unit

  (** [length c] is the number of bindings held by [c]. *)
  val length : 'a t -> int

  (** [capacity c] is the number of bindings [c] can hold:
      [capacity (create n) = n] *)
  val capacity : 'a t -> int

end

module type MAP_OPTION = sig

  (** Similar to [MAP] but the promises resolve to meaningful [option]:
      When a promise resolve to [None] it is interpreted as a failure by the
      cache and the binding is removed. When a promise resolve to [Some _] is
      interpreted as success and the binding is kept. *)

  (** The type of keys on which values in the cache are indexed. *)
  type key

  (** The type of caches holding bindings from [key] to ['a option Lwt.t] *)
  type 'a t

  (** [create n] creates a cache with a size-bound of [n]. Remember that the
      size-bound is not upheld strictly by all caches. *)
  val create : int -> 'a t

  (** [put c k p] binds the key [k] to the promise [p] in the cache [c].
      This transfer the responsibility for [p] to [c]: [c] will cancel the
      promise if it is ever [remove]d, [clear]ed, [put], etc.

      If [k] is already bound to a promise [p'] in [c], the previous binding
      disappears and is replaced by the new binding to [p]. If this happens then
      [p'] is canceled.

      If [k] is not already bound in [c], then [put] adds a new binding from
      [k] to [p]. This may or may not cause another binding to be removed from
      the cache, depending on the number of bindings already present in the
      cache [c], the size-bound of the cache [c], and the policy of the cache
      [c] towards its size-bound. If a supernumerary binding is removed, its
      promise is canceled. *)
  val put : 'a t -> key -> 'a option Lwt.t -> unit

  (** [take c k] removes the binding of [k] from [c] and returns it to the
      caller along with the responsibility for the promise: the cache will not
      cancel the promise.

      If [k] is not bound in [c], then [take c k] does nothing. *)
  val take : 'a t -> key -> 'a option Lwt.t option

  (** [take_all c] *)
  val take_all : 'a t -> (key * 'a option Lwt.t) list

  (** [take_some c f] *)
  val take_some : 'a t -> (key -> bool) -> (key * 'a option Lwt.t) list

  (** [bind c k f] waits for the promise bound to [k] in [c] to resolve and
      applies [f] to the resolved value. In more hand-wavy words: [bind c k f]
      is similar to [Lwt.bind (find c k) f].

      If [k] isn't bound in [c], then it returns [None].

      When you call [bind c k f] you take shared-responsibility for the promise.
      From this there is an important remark:

      The promise's ownership is shared. For this reason the cache will not
      cancel the promise, even if the key-promsie binding is removed from the
      cache because a supernumerary binding is inserted. If this happens the
      cache simply lets go of the promise and you become the sole owner. There
      is no mechanism to be notified that you become the sole owner. If you need
      stricter rules of ownership you need to restrict yourself to using [put]
      and [take] only.

      Note that the cache is designed to gracefully handle cancelation of the
      promises inside. And so you can cancel a promise you called [bind] on. *)
  val bind : 'a t -> key -> ('a option -> 'b Lwt.t) -> 'b Lwt.t option

  (** [bind_or_put c k mk f] is identical to [bind c k f] if [k] is bound in
      [c].

      If [k] is not bound in [c], then the promise [mk k] is created. It is
      bound to [k] in [c]. Its responsibility is shared by the cache and the
      caller as per the documentation of [bind] above. *)
  val bind_or_put : 'a t -> key -> (key -> 'a option Lwt.t) -> ('a option -> 'b Lwt.t) -> 'b Lwt.t

  (** [fold f c init] folds the function [f] and value [init] over the keys and
      values that the bindings of [c] resolve to. Bindings with promises
      resolving to [None] are ignored.

      Note that for some caches, this function may fold over a subset of the
      bindings of [c]. Specifically, on caches with a [Weak] overflow policy,
      only the strongly-held elements are folded over. *)
  val fold : (key -> 'a -> 'b -> 'b Lwt.t) -> 'a t -> 'b -> 'b Lwt.t

  (** [fold_oldest_first] is like [fold] but in reversed order: oldest elements
      of [c] first. This function has the same limitation as [fold]. *)
  val fold_oldest_first : (key -> 'a -> 'b -> 'b Lwt.t) -> 'a t -> 'b -> 'b Lwt.t

  (** [remove c k] removes the binding from [k] in [c] and cancels its promise
      (if it hasn't resolved yet).

      If [k] is not bound in [c], it does nothing. *)
  val remove : 'a t -> key -> unit

  (** [clear c] removes all bindings from [c], canceling all unresolved promises. *)
  val clear : 'a t -> unit

  (** [filter c f] *)
  val filter : 'a t -> (key -> bool) -> unit

  (** [length c] is the number of bindings held by [c]. *)
  val length : 'a t -> int

  (** [capacity c] is the number of bindings [c] can hold:
      [capacity (create n) = n] *)
  val capacity : 'a t -> int

end

module type MAP_RESULT = sig

  (** Similar to [MAP] but the promises resolve to meaningful [result]:
      When a promise resolve to [Error] it is interpreted as a failure by the
      cache and the binding is removed. When a promise resolve to [Ok _] is
      interpreted as success and the binding is kept. *)

  (** The type of keys on which values in the cache are indexed. *)
  type key

  (** The type of caches holding bindings from [key] to [('a, 'error) result Lwt.t] *)
  type ('a, 'error) t

  (** [create n] creates a cache with a size-bound of [n]. Remember that the
      size-bound is not upheld strictly by all caches. *)
  val create : int -> ('a, 'error) t

  (** [put c k p] binds the key [k] to the promise [p] in the cache [c].
      This transfer the responsibility for [p] to [c]: [c] will cancel the
      promise if it is ever [remove]d, [clear]ed, [put], etc.

      If [k] is already bound to a promise [p'] in [c], the previous binding
      disappears and is replaced by the new binding to [p]. If this happens then
      [p'] is canceled.

      If [k] is not already bound in [c], then [put] adds a new binding from
      [k] to [p]. This may or may not cause another binding to be removed from
      the cache, depending on the number of bindings already present in the
      cache [c], the size-bound of the cache [c], and the policy of the cache
      [c] towards its size-bound. If a supernumerary binding is removed, its
      promise is canceled. *)
  val put : ('a, 'error) t -> key -> ('a, 'error) result Lwt.t -> unit

  (** [take c k] removes the binding of [k] from [c] and returns it to the
      caller along with the responsibility for the promise: the cache will not
      cancel the promise.

      If [k] is not bound in [c], then [take c k] does nothing. *)
  val take : ('a, 'error) t -> key -> ('a, 'error) result Lwt.t option

  (** [take_all c] *)
  val take_all : ('a, 'error) t -> (key * ('a, 'error) result Lwt.t) list

  (** [take_some c f] *)
  val take_some : ('a, 'error) t -> (key -> bool) -> (key * ('a, 'error) result Lwt.t) list

  (** [bind c k f] waits for the promise bound to [k] in [c] to resolve and
      applies [f] to the resolved value. In more hand-wavy words: [bind c k f]
      is similar to [Lwt.bind (find c k) f].

      If [k] isn't bound in [c], then it returns [None].

      When you call [bind c k f] you take shared-responsibility for the promise.
      From this there is an important remark:

      The promise's ownership is shared. For this reason the cache will not
      cancel the promise, even if the key-promsie binding is removed from the
      cache because a supernumerary binding is inserted. If this happens the
      cache simply lets go of the promise and you become the sole owner. There
      is no mechanism to be notified that you become the sole owner. If you need
      stricter rules of ownership you need to restrict yourself to using [put]
      and [take] only.

      Note that the cache is designed to gracefully handle cancelation of the
      promises inside. And so you can cancel a promise you called [bind] on. *)
  val bind : ('a, 'error) t -> key -> (('a, 'error) result -> 'b Lwt.t) -> 'b Lwt.t option

  (** [bind_or_put c k mk f] is identical to [bind c k f] if [k] is bound in
      [c].

      If [k] is not bound in [c], then the promise [mk k] is created. It is
      bound to [k] in [c]. Its responsibility is shared by the cache and the
      caller as per the documentation of [bind] above. *)
  val bind_or_put : ('a, 'error) t -> key -> (key -> ('a, 'error) result Lwt.t) -> (('a, 'error) result -> 'b Lwt.t) -> 'b Lwt.t

  (** [fold f c init] folds the function [f] and value [init] over the keys and
      values that the bindings of [c] resolve to. Bindings with promises
      resolving to [None] are ignored.

      Note that for some caches, this function may fold over a subset of the
      bindings of [c]. Specifically, on caches with a [Weak] overflow policy,
      only the strongly-held elements are folded over. *)
  val fold : (key -> 'a -> 'b -> 'b Lwt.t) -> ('a, 'error) t -> 'b -> 'b Lwt.t

  (** [fold_oldest_first] is like [fold] but in reversed order: oldest elements
      of [c] first. This function has the same limitation as [fold]. *)
  val fold_oldest_first : (key -> 'a -> 'b -> 'b Lwt.t) -> ('a, 'error) t -> 'b -> 'b Lwt.t

  (** [remove c k] removes the binding from [k] in [c] and cancels its promise
      (if it hasn't resolved yet).

      If [k] is not bound in [c], it does nothing. *)
  val remove : ('a, 'error) t -> key -> unit

  (** [clear c] removes all bindings from [c], canceling all unresolved promises. *)
  val clear : ('a, 'error) t -> unit

  (** [filter c f] *)
  val filter : ('a, 'error) t -> (key -> bool) -> unit

  (** [length c] is the number of bindings held by [c]. *)
  val length : ('a, 'error) t -> int

  (** [capacity c] is the number of bindings [c] can hold:
      [capacity (create n) = n] *)
  val capacity : ('a, 'error) t -> int

end
