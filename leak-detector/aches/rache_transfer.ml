(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Keys = struct
  type t = int
  let equal = Int.equal
  let hash = Hashtbl.hash
end

module Resource = struct
  module Registry = Set.Make(String)
  let registry = ref Registry.empty
  let counter = ref 1
  let check_is_empty () = Registry.is_empty !registry
  let reset () = registry := Registry.empty; counter := 1
  let gen =
    fun () ->
      incr counter;
      let r = string_of_int !counter in
      registry := Registry.add r !registry;
      r
  let destroy r =
    assert (Registry.mem r !registry);
    registry := Registry.remove r !registry
end

let main ~seed ?(reach = 100) ?(size = 50) ?(repeat = 3000) () name (module C : Aches.Rache.TRANSFER with type key = Keys.t) =
  let prng = Random.State.make [| seed |] in
  if reach < 1 then raise (Invalid_argument "main: reach too low");
  let key () = Random.State.int prng reach in
  if size < 3 then raise (Invalid_argument "main: size too low");
  if repeat <= 0 then raise (Invalid_argument "main: repeat too low");
  Printf.printf "leak-detector: rache(transfer) (%s) (seed: %d) (size: %d) (repeat: %d)…" name seed size repeat;
  let () = Resource.reset () in
  let c = C.create (fun _k r -> Resource.destroy r) size in
  let rec loop max_reachable_words u v repeat =
    assert (C.length c <= size);
    let reachable_words = Obj.reachable_words (Obj.repr c) in
    let max_reachable_words = max max_reachable_words reachable_words in
    if repeat <= 0 then begin
      C.clear c;
      Resource.destroy u;
      Resource.destroy v;
      max_reachable_words
    end else
      match Random.State.int prng 8 with
      | 0 | 1 | 2 | 3 ->
          let w = Resource.gen () in
          let () = C.put c (key ()) w in
          loop max_reachable_words u v (repeat - 1)
      | 4 -> begin
          let w =
            let f = ref true in
            C.take_some c (fun _ _ -> if !f then (f := false; true) else false)
          in
          match w with
          | [] -> loop max_reachable_words u v (repeat - 1)
          | [(_, w)] ->
                Resource.destroy u;
                loop max_reachable_words v w (repeat - 1)
          | _ -> assert false
      end
      | 5 ->
          let w = Resource.gen () in
          Resource.destroy u;
          loop max_reachable_words v w (repeat - 1)
      | 6 ->
          let w = C.take c (key ()) in
          Option.iter Resource.destroy w;
          loop max_reachable_words u v (repeat - 1)
      | 7 ->
          let _ : Bytes.t option = C.borrow c (key ()) Bytes.of_string in
          loop max_reachable_words u v (repeat - 1)
      | _ -> assert false
  in
  let u = Resource.gen () in
  let v = Resource.gen () in
  match loop 0 u v repeat with
  | exception exc ->
      Printf.printf " error (%s)\n" (Printexc.to_string exc); exit 1
  | max_reachable_words ->
      if (max_reachable_words < (size * 20) + 100) then (* magic constant from observations *)
        if Resource.check_is_empty () then
          Printf.printf " ok (maximum reachable-words: %d)\n" max_reachable_words
        else begin
          Printf.printf " LEAKY resources!\n";
          exit 1
        end
      else begin
        Printf.printf " LEAKY memory!\n";
        exit 1
      end


let () =
  let seed = Option.map int_of_string (Sys.getenv_opt "LEAKSEED") in
  let seed = match seed with
    | Some seed -> seed
    | None ->
        let seederprng = Random.State.make_self_init () in
        Random.State.int seederprng 0xff_ff_ff
  in
  let reach = Option.map int_of_string (Sys.getenv_opt "LEAKREACH") in
  let size = Option.map int_of_string (Sys.getenv_opt "LEAKSIZE") in
  let repeat = Option.map int_of_string (Sys.getenv_opt "LEAKREPEAT") in
  let replacement : (string * (module Aches.Rache.REPLACEMENT)) list =
    match Sys.getenv_opt "LEAKREPLACEMENT" with
    | None | Some "all" -> [ ("LRU", (module Aches.Rache.LRU)); ("FIFO", (module Aches.Rache.FIFO)); ]
    | Some "LRU" -> [ ("LRU", (module Aches.Rache.LRU)); ]
    | Some "FIFO" -> [ ("FIFO", (module Aches.Rache.FIFO)); ]
    | _ -> raise (Invalid_argument "main: invalid LEAKREPLACEMENT (expected all, LRU, or FIFO")
  in
  List.iter (fun (name, (module R: Aches.Rache.REPLACEMENT)) ->
    let (module C : Aches.Rache.TRANSFER with type key = Keys.t) = (module Aches.Rache.Transfer(R)(Keys)) in
    main ~seed ?reach ?size ?repeat () name (module C)
  ) replacement

