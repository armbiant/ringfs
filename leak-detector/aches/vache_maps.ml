(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Keys = struct
  type t = int
  let equal = Int.equal
  let hash = Hashtbl.hash
end

let main ~seed ?(reach = 100) ?(size = 50) ?(repeat = 3000) () name (module C : Aches.Vache.MAP with type key = Keys.t) =
  let prng = Random.State.make [| seed |] in
  if reach < 1 then raise (Invalid_argument "main: reach too low");
  let key () = Random.State.int prng reach in
  if size < 3 then raise (Invalid_argument "main: size too low");
  if repeat <= 0 then raise (Invalid_argument "main: repeat too low");
  Printf.printf "leak-detector: vache(maps) (%s) (seed: %d) (size: %d) (repeat: %d)…" name seed size repeat;
  let c = C.create size in
  let rec loop max_reachable_words repeat =
    assert (C.length c <= size + 1); (* in sloppy mode some values can stick around *)
    let reachable_words = Obj.reachable_words (Obj.repr c) in
    let max_reachable_words = max max_reachable_words reachable_words in
    if repeat <= 0 then
      max_reachable_words
    else
      match Random.State.int prng 8 with
      | 0 | 1 | 2 | 3 | 4 | 5 ->
          let () = C.replace c (key ()) (Bytes.create 1) in
          loop max_reachable_words (repeat - 1)
      | 6 ->
          let () = C.remove c (key ()) in
          loop max_reachable_words (repeat - 1)
      | 7 ->
          let _ : Bytes.t option = C.find_opt c (key ()) in
          loop max_reachable_words (repeat - 1)
      | _ -> assert false
  in
  match loop 0 repeat with
  | exception exc ->
      Printf.printf " error (%s)\n" (Printexc.to_string exc); exit 1
  | max_reachable_words ->
      if (max_reachable_words < (size * 30) + 100) then begin (* magic constant from observations *)
        Printf.printf " ok (maximum reachable-words: %d)\n" max_reachable_words;
        true
      end else begin
        Printf.printf " LEAKY (maximum reachable-words: %d)!\n" max_reachable_words;
        false
      end

let cartprod xs ys =
  (* List.concat_map is only since ocaml.4.10 or so. So we use [concat (map …)].
     The lists are small anyway. *)
  List.concat (List.map (fun x -> List.map (fun y -> (x,y)) ys) xs)

let () =
  let seed = Option.map int_of_string (Sys.getenv_opt "LEAKSEED") in
  let seed = match seed with
    | Some seed -> seed
    | None ->
        let seederprng = Random.State.make_self_init () in
        Random.State.int seederprng 0xff_ff_ff
  in
  let reach = Option.map int_of_string (Sys.getenv_opt "LEAKREACH") in
  let size = Option.map int_of_string (Sys.getenv_opt "LEAKSIZE") in
  let repeat = Option.map int_of_string (Sys.getenv_opt "LEAKREPEAT") in
  let replacement =
    match Sys.getenv_opt "LEAKREPLACEMENT" with
    | None | Some "all" -> [ `LRU; `FIFO ]
    | Some "LRU" -> [ `LRU ]
    | Some "FIFO" -> [ `FIFO ]
    | _ -> raise (Invalid_argument "main: invalid LEAKREPLACEMENT (expected all, LRU, or FIFO")
  in
  let accounting =
    match Sys.getenv_opt "LEAKACCOUNTING" with
    | None | Some "all" -> [ `Precise; `Sloppy ]
    | Some "Precise" -> [ `Precise ]
    | Some "Sloppy" -> [ `Sloppy ]
    | _ -> raise (Invalid_argument "main: invalid LEAKACCOUNTING (expected all, Precise, or Sloppy")
  in
  let replacement_and_accounting : (string * (module Aches.Vache.REPLACEMENT_AND_ACCOUNTING)) list =
    List.map
      (function
        | (`LRU, `Precise) -> ("LRU-Precise", (module Aches.Vache.LRU_Precise : Aches.Vache.REPLACEMENT_AND_ACCOUNTING))
        | (`LRU, `Sloppy) -> ("LRU-Sloppy", (module Aches.Vache.LRU_Sloppy : Aches.Vache.REPLACEMENT_AND_ACCOUNTING))
        | (`FIFO, `Precise) -> ("FIFO-Precise", (module Aches.Vache.FIFO_Precise : Aches.Vache.REPLACEMENT_AND_ACCOUNTING))
        | (`FIFO, `Sloppy) -> ("FIFO-Sloppy", (module Aches.Vache.FIFO_Sloppy : Aches.Vache.REPLACEMENT_AND_ACCOUNTING))
      )
      (cartprod replacement accounting)
  in
  let overflow : (string * (module Aches.Vache.OVERFLOW)) list =
    match Sys.getenv_opt "LEAKOVERFLOW" with
    | None | Some "all" -> [ ("Strong", (module Aches.Vache.Strong)); ("Weak", (module Aches.Vache.Weak)); ]
    | Some "Strong" -> [ ("Strong", (module Aches.Vache.Strong)); ]
    | Some "Weak" -> [ ("Weak", (module Aches.Vache.Weak)); ]
    | _ -> raise (Invalid_argument "main: invalid LEAKOVERFLOW (expected all, Strong, or Weak")
  in
  let passes = ref true in
  List.iter (fun (repl_acc_name, (module RA: Aches.Vache.REPLACEMENT_AND_ACCOUNTING)) ->
    List.iter (fun (overf_name, (module O: Aches.Vache.OVERFLOW)) ->
      let module C : Aches.Vache.MAP with type key = Keys.t = Aches.Vache.Map(RA)(O)(Keys) in
      let name = Printf.sprintf "%s-%ss" repl_acc_name overf_name in
      let r = main ~seed ?reach ?size ?repeat () name (module C) in
      passes := r && !passes
    ) overflow
  ) replacement_and_accounting;
  if !passes then exit 0 else exit 1

