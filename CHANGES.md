v1.0.0:
  - Separate value caches and resource caches. The two distinct intended use
    are now given separate names and signatures. Resource caches are careful
    about resource ownership.
  - Switch from First Class Module API to functor API.
  - Note that this version is a complete rewrite with new package names, new
    module names, new APIs, new concepts. These changes are too numerous to be
    listed here: only the most important feature change is listed above. Refer
    to the README and the documentation which contain up-to-date information on
    the library as it stands.
  - Compatible with OCaml 5.0.0. The previous version used functions of the
    Ephemeron module that were removed in OCaml version 5.

--------------------------------------------------------------------------------

v0.9:
  - Provide `Collection.length`

v0.8:
  - Provide `Collection.fold_older_first`
  - Documentation fixes

v0.7:
  - Add `filter` to caches
  - Support for empty and singleton caches in ringo-lwt

v0.6:
  - Add specialised 0-sized and 1-sized caches
  - Improved test coverage
  - Improved documentation

v0.5:
  - Fix leakiness of Lwt-wrapped caches (clean-up functions would hold onto bindings)
  - Fix race condition in Lwt-wrapped caches (rejection of a non-held promise cannot cause removal of a held promise)
  - Folding in Lwt-wrapped caches ignores failed promises (aligns semantics with automatic cleaning)
  - Improve documentation of Lwt-wrapped caches

v0.4:
  - Handle reinsertion as promotion
  - bugfix: do not hard-fail when removing the single element of a collection
  - Wrappers for Lwt, Lwt-Option, and Lwt-Result
  - rename Strict to Strong (as opposed to Weak)

v0.3:
  - Rename `Loose` as `Weak`
  - Rename `CACHE` as `CACHE_MAP` (and associated name changes) to hint at key-value storage
  - introduce set-caches (`CACHE_SET`) which are simple value caches (not key-value caches)
  - introduce `clear` to empty caches entirely
  - fix singleton-collection bug
  - simplify code in main entry module

v0.2:
  - complete rewrite: the library pivots towards caches

v0.1: Initial release

Archaeology: Raw import from the Tezos project
